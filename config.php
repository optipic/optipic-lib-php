<?php

/**
 * Config of OptiPic.io plugin
 * 
 * @author https://optipic.io/
 * @copyright (c) 2018, https://optipic.io
 */

$optipicCfg = array(
    "secretkey" => "",
    "api_url" => "https://optipic.io/api/",
);
?>